require 'rails_helper'

RSpec.describe Application, type: :model do
  it { should belong_to(:job) }
  # Validation test
  it { should validate_presence_of(:user) }
end