require 'rails_helper'

RSpec.describe 'Applications API' do
  # Initialize the test data

  let(:user) { create(:user) }
  let!(:application) { create(:application, user: user.id) }
  let!(:applications) { create_list(:application, 20, job_id: todo.id) }
  let(:job_id) { todo.id }
  let(:id) { applications.first.id }
  let(:headers) { valid_headers }

  describe 'GET /jobs/:job_id/applications' do
    before { get "/jobs/#{job_id}/applications", params: {}, headers: headers }

  let!(:job) { create(:job) }
  let!(:applications) { create_list(:application, 20, job_id: job.id) }
  let(:job_id) { job.id }
  let(:id) { applications.first.id }





  # Test suite for GET /jobs/:job_id/applications
  describe 'GET /jobs/:job_id/applications' do
    before { get "/jobs/#{job_id}/applications" ,  params: {}, headers: headers }

    context 'when job exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all job applications' do
        expect(json.size).to eq(20)
      end
    end

    context 'when job does not exist' do
      let(:job_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Job/)
      end
    end
  end

  # Test suite for GET /jobs/:job_id/applications/:id
  describe 'GET /jobs/:job_id/applications/:id' do
    before { get "/jobs/#{job_id}/applications/#{id}" }

    context 'when job application exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the application' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when job application does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application/)
      end
    end
  end

  # Test suite for PUT /jobs/:job_id/applications
  describe 'POST /jobs/:job_id/applications' do
    let(:valid_attributes) { { user: 'Visit Narnia', seen: false }.to_json }

    context 'when request attributes are valid' do
      before { post "/jobs/#{job_id}/applications", params: valid_attributes, headers: headers }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/jobs/#{job_id}/applications", params: {} }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Name can't be blank/)
      end
    end
  end

  # Test suite for PUT /jobs/:job_id/applications/:id
  describe 'PUT /jobs/:job_id/applications/:id' do
    let(:valid_attributes) { { user: 'Mozart' }.to_json }

    before { put "/jobs/#{job_id}/applications/#{id}", params: valid_attributes , headers: headers}

    context 'when application exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the application' do
        updated_application = Application.find(id)
        expect(updated_application.user).to match(/Mozart/)
      end
    end

    context 'when the application does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Application/)
      end
    end
  end

  # Test suite for DELETE /jobs/:id
  describe 'DELETE /jobs/:id' do
    before { delete "/jobs/#{job_id}/applications/#{id}" , params: {}, headers: headers}

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end