FactoryBot.define do
  factory :application do
    user { Faker::StarWars.character }
    seen {false}
    job_id {nil}
  end
end