class Job < ApplicationRecord
  has_many :applications, dependent: :destroy

  # validations
  validates_presence_of :title, :description
end
